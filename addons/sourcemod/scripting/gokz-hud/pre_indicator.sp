/*
	Shows KZTimer players' PreVelMod & PreTickCounter.
*/


#define PRE_VELMOD_MAX 1.104

static Handle preIndicatorSynchronizer;

static float preVelMod[MAXPLAYERS + 1];
static float preVelModLastChange[MAXPLAYERS + 1];
static int preTickCounter[MAXPLAYERS + 1];
static bool preIndicatorShowed[MAXPLAYERS + 1];

static int botBottonInput[MAXPLAYERS + 1];



// =====[ EVENTS ]=====

void OnPluginStart_PreIndicator()
{
	preIndicatorSynchronizer = CreateHudSynchronizer();
}

void OnOptionChanged_PreIndicator(int client)
{
	ClearPreIndicator(KZPlayer(client));
}


void OnPlayerRunCmdPost_PreIndicator(int client, HUDInfo info)
{
	UpdatePreIndicator(client, info);
}

void OnCountedTeleport_PreIndicator(int client)
{
	ResetPrestrafeVelMod(client);
}


// =====[ PRIVATE ]=====

static void UpdatePreIndicator(int client, HUDInfo info)
{
	KZPlayer player = KZPlayer(client);
	KZPlayer targetPlayer = KZPlayer(info.ID);

	if (player.Alive)
	{
		CalcPrestrafeVelMod(player);
		if (player.Fake)
		{
			return;
		}
	}
	else if (targetPlayer.Valid && targetPlayer.Fake)
	{
		botBottonInput[targetPlayer.ID] = info.Buttons;
	}

	if (GOKZ_GetCoreOption(client, Option_Mode) != Mode_KZTimer || (targetPlayer.Valid && GOKZ_GetCoreOption(targetPlayer.ID, Option_Mode) != Mode_KZTimer))
    {
        if (preIndicatorShowed[client])
        {
            ClearPreIndicator(player);
        }
        return;
    }
	ShowPreIndicator(player, info);
}

static void ShowPreIndicator(KZPlayer player, HUDInfo info)
{
	switch (player.PreIndicator)
	{
		case PreIndicator_Bottom:
		{
			if (preTickCounter[info.ID] < 73)
			{
				SetHudTextParams(-1.0, 0.9, 1.0, 255, 255, 255, 0, 0, 1.0, 0.0, 0.0);
			}
			else
			{
				SetHudTextParams(-1.0, 0.9, 1.0, 230, 20, 20, 0, 0, 1.0, 0.0, 0.0);
			}
			ShowSyncHudText(player.ID, preIndicatorSynchronizer, "%.0f\n(%d)", preVelMod[info.ID] * 250, preTickCounter[info.ID]);
		}
		case PreIndicator_HealthAndArmor:
		{
			SetEntityHealth(player.ID, RoundToNearest(1 + 950 * (preVelMod[info.ID] - 1)));
			SetEntProp(player.ID, Prop_Data, "m_ArmorValue", preTickCounter[info.ID]);
		}
	}
	preIndicatorShowed[player.ID] = true;
}

static void ClearPreIndicator(KZPlayer player)
{
	SetEntityHealth(player.ID, 100);
	SetEntProp(player.ID, Prop_Data, "m_ArmorValue", 100);
	ClearSyncHud(player.ID, preIndicatorSynchronizer);
}

// implement from gokz-mode-kztimer.sp
static void CalcPrestrafeVelMod(KZPlayer player)
{
	if (!player.OnGround)
	{
		return;
	}

	int buttons = player.Buttons;
	if (player.Fake)
	{
		buttons = botBottonInput[player.ID];
	} 
	
	if (!player.Turning)
	{
		if (GetEngineTime() - preVelModLastChange[player.ID] > 0.2)
		{
			preVelMod[player.ID] = 1.0;
			preVelModLastChange[player.ID] = GetEngineTime();
		}
	}
	else if ((buttons & IN_MOVELEFT || buttons & IN_MOVERIGHT) && player.Speed > 248.9)
	{
		float increment = 0.0009;
		if (preVelMod[player.ID] > 1.04)
		{
			increment = 0.001;
		}
		
		bool forwards = GetClientMovingDirection(player.ID, false) > 0.0;
		
		if ((buttons & IN_MOVERIGHT && player.TurningRight || player.TurningLeft && !forwards)
			 || (buttons & IN_MOVELEFT && player.TurningLeft || player.TurningRight && !forwards))
		{
			preTickCounter[player.ID]++;
			
			if (preTickCounter[player.ID] < 75)
			{
				preVelMod[player.ID] += increment;
				if (preVelMod[player.ID] > PRE_VELMOD_MAX)
				{
					if (preVelMod[player.ID] > PRE_VELMOD_MAX + 0.007)
					{
						preVelMod[player.ID] = PRE_VELMOD_MAX - 0.001;
					}
					else
					{
						preVelMod[player.ID] -= 0.007;
					}
				}
				preVelMod[player.ID] += increment;
			}
			else
			{
				preVelMod[player.ID] -= 0.0045;
				preTickCounter[player.ID] -= 2;
				
				if (preVelMod[player.ID] < 1.0)
				{
					preVelMod[player.ID] = 1.0;
					preTickCounter[player.ID] = 0;
				}
			}
		}
		else
		{
			preVelMod[player.ID] -= 0.04;
			
			if (preVelMod[player.ID] < 1.0)
			{
				preVelMod[player.ID] = 1.0;
			}
		}
		
		preVelModLastChange[player.ID] = GetEngineTime();
	}
	else
	{
		preTickCounter[player.ID] = 0;
	}
}

static void ResetPrestrafeVelMod(int client)
{
	preVelMod[client] = 1.0;
	preTickCounter[client] = 0;
}

static float GetClientMovingDirection(int client, bool ladder)
{
	float fVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", fVelocity);
	
	float fEyeAngles[3];
	GetClientEyeAngles(client, fEyeAngles);
	
	if (fEyeAngles[0] > 70.0)fEyeAngles[0] = 70.0;
	if (fEyeAngles[0] < -70.0)fEyeAngles[0] = -70.0;
	
	float fViewDirection[3];
	
	if (ladder)
	{
		GetEntPropVector(client, Prop_Send, "m_vecLadderNormal", fViewDirection);
	}
	else
	{
		GetAngleVectors(fEyeAngles, fViewDirection, NULL_VECTOR, NULL_VECTOR);
	}
	
	NormalizeVector(fVelocity, fVelocity);
	NormalizeVector(fViewDirection, fViewDirection);
	
	float direction = GetVectorDotProduct(fVelocity, fViewDirection);
	if (ladder)
	{
		direction = direction * -1;
	}
	return direction;
}